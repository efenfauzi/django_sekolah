from django.contrib import admin
from .models import *

class AdminModels(admin.ModelAdmin):
	list_display = ('username','name','created','modified')

admin.site.register(Admin, AdminModels)

#admin.site.register(Pendaftaran, PendaftaranModels)