from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from datetime import datetime
from django.forms import CharField, Form, PasswordInput
import hashlib


class UserManager(BaseUserManager):
    def create_user(self, username, password=None):
        waktu = datetime.now()
        if not username:
            raise ValueError('Users must have an username address')

        admin = self.model(
            username=UserManager.normalize_email(username),
        )

        admin.set_password(password)
        admin.created = waktu.date()
        admin.modified = waktu.date()
        admin.save(using=self._db)
        return admin

    def create_superuser(self, username, password):
        admin = self.create_user(username, password=password)
        admin.is_staff = True
        admin.is_superuser = True

        admin.save(using=self._db)
        return admin

class Admin(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length=225, unique=True)
    #password = models.CharField(max_length=225, null=True, blank=True)
    name = models.CharField(max_length=225, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified = models.DateTimeField(auto_now=True, null=True, blank=True)
    is_superuser = models.BooleanField(default=False, null=True, blank=True)
    is_staff = models.BooleanField(default=False, null=True, blank=True)
    objects = UserManager()

    USERNAME_FIELD = 'username'
    
    def __unicode__(self):
       return u'%s' % (self.name)

    def get_full_name(self):
        # The user is identified by their email address
        return self.name

    def get_short_name(self):
        # The user is identified by their email address
        return self.name

    def is_superuser(self):
        return self.is_superuser

    def is_staff(self):
        return self.is_staff

    def save(self, *args, **kwargs):
        try:
            self.password = hashlib.md5(self.password).hexdigest()
            #self.password = User.set_password(self.password)
        except:
            pass
        super(Admin, self).save(*args, **kwargs)


    class Meta:
        db_table = 'admin'
        verbose_name_plural = 'Administrator'
        verbose_name = 'Administrator'



class Pendaftaran(models.Model):

    class Meta:
        db_table = 'pendaftaran'
        verbose_name_plural = 'Pendaftaran'
        verbose_name = 'Pendaftaran'
